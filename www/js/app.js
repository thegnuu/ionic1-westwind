
angular.module('westwind', ['ionic','ionic.service.core', 'ngCordova', 'uiGmapgoogle-maps', 'ngStorage'])

  .config(['$stateProvider', '$urlRouterProvider', 'uiGmapGoogleMapApiProvider', function($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider) {


    uiGmapGoogleMapApiProvider.configure({
      //    key: 'your api key',
      v: '3.20', //defaults to latest 3.X anyhow
      libraries: 'weather,geometry,visualization'
    });

    $stateProvider

      .state('login', {
        url: "/login",
        abstract: false,
        templateUrl: "templates/login.html",
        controller: "LoginController"
      })

      .state('tab', {
        url: "/tab",
        abstract: true,
        templateUrl: "templates/tabs.html"
      })

      .state('tab.home', {
        url: '/home',
        views: {
          'home-tab': {
            templateUrl: 'templates/home.html',
            controller: 'HomeController'
          }
        }
      })

      .state('tab.person', {
        url: '/person',
        views: {
          'person-tab': {
            templateUrl: 'templates/person.html',
            controller: 'AddressController'
          }
        }
      })

      .state('tab.process', {
        url: '/process',
        views: {
          'process-tab': {
            templateUrl: 'templates/process.html',
            controller: 'ProcessController'
          }
        }
      });

    $urlRouterProvider.otherwise('/login');


  }])

  .run(function($ionicPlatform, $state) {

    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if(window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }

    });

    $ionicPlatform.on('resume', function() {

    });

    $ionicPlatform.on('pause', function() {
      //Do something here on entering background
      $state.go('login');
    });

  })

  .controller('LoginController', function($scope, $timeout, $ionicPlatform, $rootScope, $cordovaTouchID, $state) {

    $scope.pin = "";
    $scope.touchIdSupported = false;


    $scope.add = function(number) {
      console.log(number);
      $scope.pin += number;

      if ($scope.pin.length == 4) {
        $timeout(function() {
          if ($scope.pin == "1234") {
            login();
          } else {
            $scope.pin = ""
          }
        }, 300);
      }
    };


    $scope.back = function(){
      if ($scope.pin.length > 0) {
        $scope.pin = $scope.pin.substr(0, $scope.pin.length - 1);
      }
    };

    function login() {
      $scope.pin = "";
      $state.go('tab.home');
    }

    // TOUCH-ID

    // check if available
    $ionicPlatform.ready(function () {
      $cordovaTouchID.checkSupport().then(function () {
        $scope.touchIdSupported = true;
        //$scope.$apply();
      }, function (error) {
        $scope.touchIdSupported = false;
        //$scope.$apply();
      });
    });

    $scope.touchId = function() {
      $ionicPlatform.ready(function() {
        $cordovaTouchID.authenticate("bbit cloud Login").then(function() {
          login();
        }, function () {
          //alert("error while authenticating");
        });
      });

    };



  })

  .controller('HomeController', function($scope, $ionicPlatform, $cordovaBarcodeScanner, StorageService, $cordovaGeolocation) {

    // BARCODE

    $scope.barcode = '';
    $scope.appVerion = "0.1.8";

    $scope.scanCode = function() {
      $ionicPlatform.ready(function() {
        $cordovaBarcodeScanner
          .scan()
          .then(function (barcodeData) {
            $scope.barcode = barcodeData
          }, function (error) {
            $scope.barcode = error;
          });
      });
    };

    // LOCAL STORAGE

    $scope.things =  {};
    $scope.getAll = function() {
      $scope.things = StorageService.getAll();
    };

    $scope.add = function (newThing) {
      StorageService.add(newThing);
    };

    $scope.removeAll = function () {
      StorageService.removeAll();
      $scope.things =  {};
    };

    // IONIC DEPLOY

    var deploy = new Ionic.Deploy();

    $scope.updateStatus = "";
    $scope.hasUpdate = false;

    // Update app code with new release from Ionic Deploy
    $scope.doUpdate = function() {
      deploy.update().then(function(res) {
        console.log('Ionic Deploy: Update Success! ', res);
        $scope.updateStatus = 'Ionic Deploy: Update Success! ' + res;
        $scope.$apply();
      }, function(err) {
        console.log('Ionic Deploy: Update error! ', err);
        $scope.updateStatus = 'Ionic Deploy: Update error! ' + err;
        $scope.$apply();
      }, function(prog) {
        console.log('Ionic Deploy: Progress... ', prog);
        $scope.updateStatus = 'Ionic Deploy: Progress... ' + prog;
        $scope.$apply();
      });
    };

    // Check Ionic Deploy for new code
    $scope.checkForUpdates = function() {
      console.log('Ionic Deploy: Checking for updates');
      deploy.check().then(function(hasUpdate) {
        console.log('Ionic Deploy: Update available: ' + hasUpdate);
        $scope.hasUpdate = hasUpdate;
        $scope.$apply();
      }, function(err) {
        console.error('Ionic Deploy: Unable to check for updates', err);
      });
    };



    // Geolocation

    $scope.position = {lat: 0, lng: 0};

    $scope.getPosition = function() {
      var posOptions = {timeout: 10000, enableHighAccuracy: false};
      $ionicPlatform.ready(function () {
        $cordovaGeolocation
          .getCurrentPosition(posOptions)
          .then(function (position) {
            $scope.position.lat = position.coords.latitude;
            $scope.position.lng = position.coords.longitude
          }, function (err) {
            $scope.position = err;
          });
      });
    }


  })

  .controller('AddressController', function($scope, $ionicPlatform, $ionicScrollDelegate, $rootScope, uiGmapGoogleMapApi, $http, mqttService) {

    $scope.selectedPerson = {};

    $scope.selectPerson = function(person) {

      $scope.selectedPerson = person;

      if (person.addresses[0].longitude.length > 0 && person.addresses[0].latitude.length > 0) {

        uiGmapGoogleMapApi.then(function (maps) {
          $scope.map = {center: coords, zoom: 15};
          $scope.marker = {
            id: 0,
            coords: {
              latitude: person.addresses[0].longitude,
              longitude: person.addresses[0].latitude
            },
            options: {
              draggable: false
            }
          }
        });

      } else {

        $http.get('http://maps.google.com/maps/api/geocode/json?address='
          + person.addresses[0].street + '+' + person.addresses[0].houseNo + ',+'
          + person.addresses[0].zip + '+' + person.addresses[0].city + '&sensor=false').success(function (mapData) {
          if (mapData.results.length > 0) {
            var lat = mapData.results[0].geometry.location.lat;
            var lng = mapData.results[0].geometry.location.lng;
            var coords = {latitude: lat, longitude: lng};

            uiGmapGoogleMapApi.then(function (maps) {
              $scope.map = {center: coords, zoom: 15};
              $scope.marker = {id: 0, coords: {latitude: lat, longitude: lng}, options: {draggable: false}}
            });
          }
        });

      }

    };

    $scope.persons = [];

    mqttService.request('/persons/1/0/0/select/list', {}, hanldeRequestedData);
    $rootScope.$on('mqtt-message', function(event, message) {

    });

    function hanldeRequestedData(message){
      $scope.persons = message.data;
      $scope.$apply();
      $ionicScrollDelegate.resize();
    }

  })

  .controller('ProcessController', function($scope, $timeout, $ionicPlatform, $cordovaMedia, $rootScope, mqttService, $ionicScrollDelegate) {

    $scope.title = "Prozesse";

    $scope.stage1Process = {};
    $scope.filterStage1 = {};
    $scope.filterStage2 = {};
    $scope.filterStage3 = {};
    $scope.filterStage1.parentId = '-';
    $scope.filterStage2.parentId = '-';

    $scope.processes = [];


    mqttService.request('/processes/1/0/0/select/list', {}, hanldeRequestedData);
    $rootScope.$on('mqtt-message', function(event, message) {


    });

    function hanldeRequestedData(message){
      $scope.processes = message.data;
      $scope.$apply();
      $ionicScrollDelegate.resize();
    }

    $scope.setStage1Process = function(item) {

      $scope.filterStage2.parentId = item.id;
      $scope.filterStage3.parentId = '';

      if($scope.stage1Process == item) {
        $scope.stage1Process = {};
        $scope.stage2Process = {};
        $scope.title = "Prozesse";
      } else {
        $scope.stage1Process = item;
        $scope.stage2Process = {};
        $scope.title = item.title;
      }


    };

    $scope.setStage2Process = function(item) {

      $scope.filterStage3.parentId = item.id;

      if($scope.stage2Process == item) {
        $scope.stage2Process = {};
        $scope.filterStage3.parentId = '';
        $scope.title = $scope.stage1Process.title
      } else {
        $scope.stage2Process = item;
        $scope.title = $scope.stage1Process.title + ' > ' + item.title;
      }


    };

    // easteregg 1
    $scope.eggCounter = 0;
    $scope.showEgg = false;
    $scope.showThatEgg = function() {
      $scope.eggCounter ++;
      if ($scope.eggCounter  == 7) {
        $scope.showEgg = true;
        $scope.eggCounter = 0;

        $timeout(function() {
          $ionicPlatform.ready(function() {
            var media = new Media('audio/egg1.mp3', null, null, null);
            media.play();
          });
        }, 1000);

        $timeout(function() {
          $scope.showEgg = false;
        }, 2000);
      }

    }

  })

.filter('getById', function() {
  return function(input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (+input[i].id == +id) {
        return input[i];
      }
    }
    return null;
  }
})

.factory ('StorageService', function ($localStorage) {

  $localStorage = $localStorage.$default({
    things: []
  });

  this.getAll = function () {
    return $localStorage.things;
  };
  this.add = function (thing) {

    if (!$localStorage.things) {
      $localStorage = $localStorage.$default({
        things: []
      });
    }

    $localStorage.things.push(thing);
  };
  this.remove = function (thing) {
    $localStorage.things.splice($localStorage.things.indexOf(thing), 1);
  };
  this.removeAll = function () {
    delete $localStorage.things;
  };

  return this
})


.factory('mqttService',
  ['$rootScope',
    function ($rootScope) {

      var self = this;
      var mqtt = {};
      var connected = false;
      var isConnecting = false;
      var host = "formula.xrj.ch";
      var port = 9001;
      var username = "web";
      var password = "1234";
      var reconnectTimeout = 2000;

      var options = {
        timeout: 10,
        useSSL: false,
        cleanSession: true,
        onSuccess: onConnect,
        onFailure: onError
      };


      function connect() {

        isConnecting = true;
        if (connected) return;

        console.log('connecting...');
        mqtt = new Paho.MQTT.Client(host, Number(port), "ios_" + generateId(5));
        mqtt.onConnectionLost = onConnectionLost;
        mqtt.onMessageArrived = onMessageArrived;

        if (username != null) {
          options.userName = username;
          options.password = password;
        }
        mqtt.connect(options);

      }

      this.subscribe = function(destination) {
        mqtt.subscribe(destination);
      };

      this.callbackHandler = {};

      this.request = function(destination, data, callback) {

        var requestId = generateId(10);

        var payload = {
          auth: "",
          data: data,
          lang: "de_ch"
        };

        self.callbackHandler[requestId] = {
          destination: destination,
          payload: JSON.stringify(payload),
          callback: callback
        };

        if (!connected) return;

        self.performRequest(requestId);


      };

      this.performRequest = function(requestId) {
        console.log("request: " + self.callbackHandler[requestId].destination);
        var message = new Paho.MQTT.Message(self.callbackHandler[requestId].payload);
        message.destinationName = "/data/req/" + requestId + self.callbackHandler[requestId].destination;
        self.subscribe("/data/ok/" + requestId + self.callbackHandler[requestId].destination);
        mqtt.send(message);
      };

      function onConnect() {
        console.log("connected!");
        $rootScope.$emit('mqtt-connected');
        connected = true;
        isConnecting = false;
        for (var id in self.callbackHandler) {
          self.performRequest(id)
        }
      }

      function onError(err) {
        setTimeout(connect, reconnectTimeout);
        console.dir(err);
      }


      function onConnectionLost(err) {
        if (err.errorCode !== 0) {
          connected = false;
          $rootScope.$emit('mqtt-connection-lost', err);
          console.log("connection lost!" + err.errorMessage);
        }
      }

      function onMessageArrived(message) {
        message.data = JSON.parse(message.payloadString).data;
        var id = message.destinationName.split("/")[3];
        if (self.callbackHandler.hasOwnProperty(id)) {
          self.callbackHandler[id].callback(message);
          delete self.callbackHandler[id];
        } else {
          $rootScope.$emit('mqtt-message', message);
        }
      }

      function generateId(length) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for( var i=0; i < length; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
      }

      connect();

      return this;

    }
  ]
);







/*$scope.clearSearch = function() {
 $scope.data.searchQuery = '';
 };*/
